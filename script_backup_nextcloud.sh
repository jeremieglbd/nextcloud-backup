#!/bin/bash

backup_date=$(date +"%Y%m%d")

# Directories

nextcloud_dir=/var/www/html/nextcloud
nextcloud_backup_dir=/nextcloud_backup
nextcloud_data_dir=/run/media/nextcloud_data

# Files

nextcloud_backup=$nextcloud_backup_dir/nextcloud_backup_$backup_date.tar.gz
nextcloud_db_backup=$nextcloud_backup_dir/nextcloud_db_$backup_date.sql
nextcloud_data_backup=$nextcloud_backup_dir/nextcloud_data_$backup_date.tar.gz

# DB Backup
db_server=localhost
db_username=nextcloud

# Logs

backup_logs=$nextcloud_backup_dir/backup_$backup_date.log

# delete backups older than 10 days

#find $nextcloud_backup_dir -mtime +10 -type f -delete

cd $nextcloud_dir/
sudo -u www-data php occ maintenance:mode --on

# Backup Nextcloud directory

sudo rsync -Aavx --delete $nextcloud_dir/ $nextcloud_backup_dir/nextcloud_backup \
  && sudo tar czf $nextcloud_backup \
  -C $nextcloud_backup_dir/ nextcloud_backup/ \
  || echo "nextcloud directory backup failed $backup_date: archive not created" | \
  sudo tee $backup_logs

tar ztvf $nextcloud_backup > /dev/null \
  && sha256sum $nextcloud_backup | \
  sudo tee $nextcloud_backup.sha256 \
  || echo "nextcloud directory backup failed $backup_date: archive corrupted or not created" | \
  sudo tee $backup_logs

# Backup MySQL database

# in order to use this command, set a ~/.my.cnf file with the content below :
## [mysqldump]
## user=mysqluser
## password=sercret

sudo mysqldump --single-transaction -h $db_server -u $db_username nextcloud > \
  $nextcloud_db_backup

sha256sum $nextcloud_db_backup | \
  sudo tee $nextcloud_db_backup.sha256 \
  || echo "nextcloud database backup failed $backup_date" | \
  sudo tee $backup_logs

# Backup Nextcloud data

sudo rsync -Aavx --delete $nextcloud_data_dir/ $nextcloud_backup_dir/nextcloud_data \
  && sudo tar czf $nextcloud_data_backup \
  -C $nextcloud_backup_dir/ nextcloud_data/ \
  || echo "nextcloud data backup failed $backup_date: archive not created" | \
  sudo tee $backup_logs

tar ztvf $nextcloud_data_backup > /dev/null \
  && sha256sum $nextcloud_data_backup | \
  sudo tee $nextcloud_data_backup.sha256 \
  || echo "nextcloud data backup failed $backup_date: archive corrupted or not created" | \
  sudo tee $backup_logs

cd $nextcloud_dir
sudo -u www-data php occ maintenance:mode --off
